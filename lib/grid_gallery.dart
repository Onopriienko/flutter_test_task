import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'screen_for_each_image.dart';

class GridGallery extends StatefulWidget {
  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<GridGallery> {
  late bool _isDataReady;
  bool _allDataLoaded = false;
  bool _allSharedPreferencesDataLoaded = false;

  final String _apiKey = '23102807-4715787aa15a9c734e301bef6';
  int _pageNumber = 1;
  List<String> images = [];

  late ScrollController _scrollController;

  _saveData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList("images", images);
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange &&
        _scrollController.position.userScrollDirection ==
            ScrollDirection.reverse) {
      setState(() {
        if (images.length == 100) {
          if (_allSharedPreferencesDataLoaded == false) {
            _saveData();
            _allSharedPreferencesDataLoaded = true;
          }
          _allDataLoaded = true;
        } else {
          _pageNumber++;
          getImages();
        }
      });
    }
  }

  void getImages() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final storagedImages = prefs.getStringList('images');
    if (storagedImages != [] &&
        storagedImages != null &&
        _allSharedPreferencesDataLoaded == false) {
      images = storagedImages;
      _allSharedPreferencesDataLoaded = true;
    } else {
      setState(() {
        _isDataReady = false;
      });
    }
    Future getData() async {
      final String url =
          'https://pixabay.com/api/?key=$_apiKey&image_type=all&per_page=10&page=$_pageNumber';

      http.Response response;

      try {
        response = await http.get(Uri.parse(url));
      } catch (_) {
        return;
      }
      if (response.statusCode == 200) {
        String data = response.body;
        return jsonDecode(data);
      }
    }

    final dynamic data;
    data = await getData();
    if (images.length < 100) {
      for (var i = 0; i < 10; i++) {
        images.add(data['hits'][i]['largeImageURL']);
      }
    }
    setState(() {
      _isDataReady = true;
    });
  }

  @override
  void initState() {
    super.initState();

    setState(() {
      _isDataReady = false;
    });

    _scrollController = new ScrollController()..addListener(_scrollListener);

    getImages();
  }

  circularProgress() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Test Task'),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.all(5),
        child:
            CustomScrollView(controller: _scrollController, slivers: <Widget>[
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, crossAxisSpacing: 5.0, mainAxisSpacing: 5.0),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return CachedNetworkImage(
                  imageUrl: images[index],
                  imageBuilder: (context, image) => GestureDetector(
                    onTap: () {
                      Route route = MaterialPageRoute(
                          builder: (context) => EachImage(image: image));
                      Navigator.push(context, route);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: image,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Center(
                    child: circularProgress(),
                  ),
                  errorWidget: (context, url, error) =>
                      Icon(Icons.broken_image),
                );
              },
              childCount: images.length,
            ),
          ),
          SliverToBoxAdapter(
            child: _allDataLoaded == true
                ? Container(
                    margin: EdgeInsets.all(5),
                    child: Text(
                      'End of story ;(',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.blue.shade800,
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                : Container(),
          ),
          SliverToBoxAdapter(
            child: _isDataReady == false ? circularProgress() : Container(),
          ),
        ]),
      ),
    );
  }
}
