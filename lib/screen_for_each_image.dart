import 'package:flutter/material.dart';

class EachImage extends StatelessWidget {
  EachImage({required this.image});
  final ImageProvider image;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chosen image"),
        centerTitle: true,
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Card(
                child: Image(
                  image: image,
                ),
                margin: EdgeInsets.only(bottom: 50.0),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FloatingActionButton.extended(
                    heroTag: "btnClose",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    label: Text("Close"),
                    icon: Icon(Icons.close),
                    backgroundColor: Colors.blue,
                  ),
                  FloatingActionButton.extended(
                    heroTag: "btnShare",
                    onPressed: () {
                      print("Share this image");
                    },
                    label: Text("Share"),
                    icon: Icon(Icons.share),
                    backgroundColor: Colors.blue,
                  ),
                ],
              )
            ],
          )),
    );
  }
}
