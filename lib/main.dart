import 'package:flutter/material.dart';
import 'grid_gallery.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test task',
      home: GridGallery(),
    );
  }
}
